#!/usr/bin/python3

import pickle

with open('emissions.data', 'rb') as fem:
    emissions = pickle.load(fem)
with open('transforms.data', 'rb') as ftr:
    transforms = pickle.load(ftr)

print('num of charas: {}'.format(len(emissions)))

# py to ch; ch single prob
pys = {}
chp = {}
for ch in emissions:
    if ch not in chp:
        chp[ch] = 0
    for py in emissions[ch]:
        chp[ch] += emissions[ch][py]
        if py not in pys:
            pys[py] = set()
        pys[py].add(ch)

# make conditional prob
for ch in emissions:
    tot = 0
    ls = emissions[ch]
    for py in ls:
        tot += ls[py]
    for py in ls:
        ls[py] = float(ls[py]) / tot

for ch1 in transforms:
    tot = 0
    ls = transforms[ch1]
    for ch2 in ls:
        tot += ls[ch2]
    for ch2 in ls:
        ls[ch2] = float(ls[ch2]) / tot

tot = 0
for ch in chp:
    tot += chp[ch]
for ch in chp:
    chp[ch] = float(chp[ch]) / tot

def get_or_default(d, key, value):
    if key in d:
        return d[key]
    return value

class Node:
    def __init__(self, py, ch, lastchs):
        self.pinyin = py
        self.charactar = ch
        self.prob = 0
        self.lastnode = None
        for lnode in lastchs:
            if lnode.prob * get_or_default(get_or_default(transforms, lnode.charactar, {}), ch, 0.000001) > self.prob:
                self.prob = lnode.prob * get_or_default(get_or_default(transforms, lnode.charactar, {}), ch, 0.000001)
                self.lastnode = lnode
        if len(lastchs) == 0:
            self.prob = chp[ch]
        self.prob *= emissions[ch][py]
    def get(self):
        if self.lastnode == None:
            return self.charactar
        return self.lastnode.get() + self.charactar

def doinputpinyin(pinyin, lastchs):
    chs = []
    for ch in pys[pinyin]:
        chnode = Node(pinyin, ch, lastchs)
        chs.append(chnode)
    return chs

def doinput(pinyins):
    chs = []
    for py in pinyins:
        chs = doinputpinyin(py, chs)
    chs.sort(key=lambda x: -x.prob)
    print(chs[0].get())

def splitpinyins(s):
    if len(s) == 0:
        return []
    if not s[0].isalpha():
        return splitpinyins(s[1:])
    for l in range(6, 0, -1):
        py = s[:l]
        if py in pys:
            ls = splitpinyins(s[l:])
            if ls != None:
                return [py] + ls
    return None

def inputs(s):
    ls = splitpinyins(s)
    print(ls)
    if ls != None:
        doinput(ls)

inputs('nihao')
inputs('jizhiruwo')
inputs('zhonghuarenmin')
inputs('gongheguo')
inputs('baiduyixianijiuzhidao')
inputs('renmindahuitang')

inputs('xianyoudebofangqiyouhenduodouhanyoubushaorongyubufen')
