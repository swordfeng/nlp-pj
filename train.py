#!/usr/bin/python3

from pypinyin import *
import os
import pickle

transforms = {}
def addTransform(c1, c2):
    if c1 not in transforms:
        transforms[c1] = {}
    if c2 not in transforms[c1]:
        transforms[c1][c2] = 1
    else:
        transforms[c1][c2] += 1

emissions = {}
def addEmission(ch, py):
    if ch not in emissions:
        emissions[ch] = {}
    if py not in emissions[ch]:
        emissions[ch][py] = 1
    else:
        emissions[ch][py] += 1

def process_text(text):
    py = lazy_pinyin(text, errors=len)
    pos = 0
    lastCharactar = False
    for it in py:
        if type(it) == int:
            # errors
            pos += it
            lastCharactar = False
        else:
            if lastCharactar:
                addTransform(text[pos-1], text[pos])
            addEmission(text[pos], it)
            pos += 1
            lastCharactar = True

def process_on_file(path):
    with open(path, 'rb') as f:
        print('read file {}'.format(path))
        buf = f.read()
        text = None
        try:
            try:
                # try decode as utf8
                text = buf.decode()
            except UnicodeDecodeError:
                text = buf.decode('gbk')
        except:
            print('failed to process {}'.format(path))
            return
        process_text(text)
        print('processed file {}'.format(path))

def process_on_directory(path):
    for (dirpath, dirnames, filenames) in os.walk(path):
        for filename in filenames:
            if filename[-4:] != '.txt':
                continue
            process_on_file(os.path.join(dirpath, filename))

process_on_directory('data')
print('num of charas: {}'.format(len(emissions)))

with open('emissions.data', 'wb') as fem:
    pickle.dump(emissions, fem)
with open('transforms.data', 'wb') as ftr:
    pickle.dump(transforms, ftr)

# py to ch
#pys = {}
#for ch in emissions:
#    for py in emissions[ch]:
#        if py not in pys:
#            pys[py] = set()
#        pys[py].add(ch)
